package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.concurrent.CountDownLatch;

@SpringBootApplication
public class LatchLesson {

    public static void main(String[] args) {
        SpringApplication.run(LatchLesson.class, args);
    }

    public static class Worker implements Runnable {
        private final List<String> outputScraper;
        private final CountDownLatch countDownLatch;

        public Worker(List<String> outputScraper, CountDownLatch countDownLatch) {
            this.outputScraper = outputScraper;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            doSomeWork();
            outputScraper.add("Counted down");
            countDownLatch.countDown();
        }

        private void doSomeWork() {
            System.out.println("Doing some hard work...");
        }
    }
}
